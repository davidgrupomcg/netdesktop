﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Net.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetDesktop.HostBuilders
{
    public static class AddDbContextHostBuilderExtensions
    {
        public static IHostBuilder AddDbContext(this IHostBuilder host)
        {
            host.ConfigureServices((context, services) =>
            {
                Action<DbContextOptionsBuilder> configureDbContext = o => o.UseInMemoryDatabase(databaseName: "Test");                 
                services.AddDbContext<NetDbContext>(configureDbContext);
            });

            return host;
        }
    }
}
