﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Net.Data.Models;
using Net.Data.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetDesktop.HostBuilders
{
    public static class AddServicesHostBuilderExtensions
    {
        public static IHostBuilder AddServices(this IHostBuilder host)
        {
            host.ConfigureServices(services =>
            {
                services.AddSingleton<IDataService<Country>, GenericDataService<Country>>();

            });

            return host;
        }
    }
}
