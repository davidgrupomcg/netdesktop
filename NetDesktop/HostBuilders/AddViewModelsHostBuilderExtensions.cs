using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Net.Data.Models;
using Net.Data.Services;
using NetDesktop.ViewModels;

namespace NetDesktop.HostBuilders
{
    public static class AddViewModelsHostBuilderExtensions
    {
        public static IHostBuilder AddViewModels(this IHostBuilder host)
        {
            host.ConfigureServices(services =>
            {
                //I think should be singleton-Review
                services.AddTransient(s => new MainWindowViewModel(s.GetRequiredService<IDataService<Country>>()));
            });

            return host;
        }
    }
}
