﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NetDesktop.ViewModels
{
   public class SimpleEditableCountry:ValidatableBindableBase
    {

        private string code;
        private string name;
        private int id;

        public int Id
        {
            get { return id; }
            set { SetProperty(ref id, value); }
        }

        [StringLength(30)]
        [Required]
        public string Code
        {
            get { return code; }
            set { SetProperty(ref code, value); }
        }


        [StringLength(30)]
        [Required]
        public string Name
        {
            get { return name; }
            set { SetProperty(ref name, value); }
        }


    }
}
