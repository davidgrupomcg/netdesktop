﻿using Net.Data.Models;
using Net.Data.Services;
using NetDesktop.Commands;
using System;


namespace NetDesktop.ViewModels
{
    public class AddEditCountryViewModel : ViewModelBase
    {
        private IDataService<Country> _repo;

        public AddEditCountryViewModel(IDataService<Country> repo)
        {
            _repo = repo;
            CancelCommand = new RelayCommand(OnCancel);
            SaveCommand = new RelayCommand(OnSave, CanSave);
        }

        public RelayCommand CancelCommand { get; private set; }
        public RelayCommand SaveCommand { get; private set; }

        public event Action Done = delegate { };

        private bool _EditMode;

        public bool EditMode
        {
            get { return _EditMode; }
            set { SetProperty(ref _EditMode, value); }
        }

        private SimpleEditableCountry _country;

        public SimpleEditableCountry Country
        {
            get { return _country; }
            set { SetProperty(ref _country, value); }
        }


        private Country _editingCountry = null;

        public void SetCustomer(Country country)
        {
            _editingCountry = country;
            if (Country != null) Country.ErrorsChanged -= RaiseCanExecuteChanged;
            Country = new SimpleEditableCountry();
            Country.ErrorsChanged += RaiseCanExecuteChanged;
            CopyCountry(country, Country);
        }

        private void RaiseCanExecuteChanged(object sender, EventArgs e)
        {
            SaveCommand.RaiseCanExecuteChanged();
        }

        private void CopyCountry(Country source, SimpleEditableCountry target)
        {
            target.Id = source.Id;

            if (EditMode)
            {
                target.Code = source.Code;
                target.Name = source.Name;               
            }
        }

        private void OnCancel()
        {
            Done();
        }

        private async void OnSave()
        {
            UpdateCustomer(Country, _editingCountry);
            if (EditMode)
                await _repo.Update(_editingCountry.Id, _editingCountry);
            else
                await _repo.Create(_editingCountry);
            Done();
        }

        private void UpdateCustomer(SimpleEditableCountry source, Country target)
        {
            target.Name = source.Name;
            target.Code = source.Code;
           
        }

        bool CanSave()
        {
            return !Country.HasErrors;
        }
    }

}
