using System;
using System.Collections.Generic;
using System.Text;

namespace NetDesktop.ViewModels
{
    public class ViewModelBase:BindableBase
    {
        //No longer needed---We could just use BindableBase in the ViewModels
        public delegate TViewModel CreateViewModel<TViewModel>() where TViewModel : ViewModelBase;

    }
}
