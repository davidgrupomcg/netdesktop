﻿using Net.Data.Models;
using Net.Data.Services;
using System;

namespace NetDesktop.ViewModels
{
    class MainWindowViewModel : ViewModelBase
    {
        private CountriesListViewModel _customerListViewModel;
        private AddEditCountryViewModel _addEditViewModel;      

        public MainWindowViewModel(IDataService<Country> repo)
        {
            _customerListViewModel = new CountriesListViewModel(repo);
            _addEditViewModel = new AddEditCountryViewModel(repo);

            CurrentViewModel = _customerListViewModel;

            _customerListViewModel.AddCountryRequested += NavToAddCustomer;
            _customerListViewModel.EditCountryRequested += NavToEditCustomer;    
            _addEditViewModel.Done += NavToCustomerList;
        }
   

        private BindableBase _CurrentViewModel;
        public BindableBase CurrentViewModel
        {
            get { return _CurrentViewModel; }
            set { SetProperty(ref _CurrentViewModel, value); }
        }        


        private void NavToAddCustomer(Country country)
        {
            _addEditViewModel.EditMode = false;
            _addEditViewModel.SetCustomer(country);
            CurrentViewModel = _addEditViewModel;
        }

        private void NavToEditCustomer(Country country)
        {
            _addEditViewModel.EditMode = true;
            _addEditViewModel.SetCustomer(country);
            CurrentViewModel = _addEditViewModel;
        }


        private void NavToCustomerList()
        {
            CurrentViewModel = _customerListViewModel;
        }
    }

}
