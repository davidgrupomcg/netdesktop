﻿using Net.Data.Models;
using Net.Data.Services;
using NetDesktop.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace NetDesktop.ViewModels
{
    public class CountriesListViewModel: ViewModelBase
    {
        private IDataService<Country> _repo;

        public CountriesListViewModel(IDataService<Country> repository)
        {
            this._repo = repository;
            AddCountryCommand = new RelayCommand(OnAddCountry);
            EditCountryCommand = new RelayCommand<Country>(OnEditCountry);
            DeleteCountryCommand = new RelayCommand<Country>(OnDeleteCountry);
        }
        

        private ObservableCollection<Country> _countries;
        public ObservableCollection<Country> Countries
        {
            get { return _countries; }
            set { SetProperty(ref _countries, value); }
        }

        public async void LoadCustomers()
        {
            await GetCountries();
        }

        public RelayCommand AddCountryCommand { get; private set; }
        public RelayCommand<Country> DeleteCountryCommand { get; private set; }       
        public RelayCommand<Country> EditCountryCommand { get; private set; }



        public event Action<Country> AddCountryRequested = delegate { };
        public event Action<Country> EditCountryRequested = delegate { };


        public async Task GetCountries()
        {
            var countries = await _repo.GetAll();           
            this.Countries = new ObservableCollection<Country>(countries);
        }
     
        private void OnEditCountry(Country obj)
        {
            EditCountryRequested(obj);
        }

        private async void OnDeleteCountry(Country obj)
        {
            await _repo.Delete(obj.Id);
            await GetCountries();
        }
        private void OnAddCountry()
        {
            AddCountryRequested(new Country());
        }      
}
}
