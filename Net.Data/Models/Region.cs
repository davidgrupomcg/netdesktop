﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Net.Data.Models
{
    public class Region : DomainObject
    {              
        public string Code { get; set; }
        public string Name { get; set; }
        public ICollection<Country> Countries { get; set; }

    }
}
