﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Net.Data.Models
{
    public class Market : DomainObject
    {   
       
        public string Code { get; set; }

        public int CountryId { get; set; }
     
        public Country Country { get; set; }
    }
}
