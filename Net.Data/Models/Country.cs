﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Net.Data.Models
{
    public class Country : DomainObject
    {            
     
        public string Code { get; set; }

        public string Name { get; set; }

        public int RegionId { get; set; }

        public int ContactId { get; set; }

        public int BackupContactId { get; set; }

        public Region Region { get; set; }
       
        public Contact Contact { get; set; }

        public Contact BackupContact { get; set; }

        public ICollection<Market> Markets { get; set; }

    }
}
