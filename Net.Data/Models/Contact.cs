﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Net.Data.Models
{
    public class Contact:DomainObject
    {   
        public string Code { get; set; }

        public string Name { get; set; }
        public ICollection<Country> ContactCountries { get; set; }
        public ICollection<Country> BackupContactCountries { get; set; }

    }
}
