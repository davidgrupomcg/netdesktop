﻿using Microsoft.EntityFrameworkCore;
using Net.Data.Models;


namespace Net.Data
{ 
    public class NetDbContext : DbContext
        {
            public DbSet<Contact> Contacts { get; set; }
            public DbSet<Country> Countries { get; set; }
            public DbSet<Market> Markets { get; set; }
            public DbSet<Region> Regions { get; set; }

        public NetDbContext(DbContextOptions options) : base(options) { }    

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Country>()
            .HasOne(p => p.Contact)
            .WithMany(b => b.ContactCountries).OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Country>()
           .HasOne(p => p.BackupContact)
           .WithMany(b => b.BackupContactCountries).OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Market>()
           .HasOne(p => p.Country)
           .WithMany(b => b.Markets);


            modelBuilder.Entity<Country>()
           .HasOne(p => p.Region)
           .WithMany(b => b.Countries);           
        }

    }
    
}
