using Microsoft.EntityFrameworkCore;
using Net.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Net.Data.Services
{
    public class GenericDataService<T> : IDataService<T> where T : DomainObject
    {
        private readonly NetDbContext dbContext;

        public GenericDataService(NetDbContext dbContext)
        {
            this.dbContext = dbContext;
            //There should be a better way of seeding an in-memory database, but haven´t found it
            DBHelper.Seed(this.dbContext);
        }

        public async Task<T> Create(T entity)
        {
              var createdResult = await dbContext.Set<T>().AddAsync(entity);
              await dbContext.SaveChangesAsync();
              return createdResult.Entity;
           
        }

        public async Task<T> Update(int id, T entity)
        {           
                entity.Id = id;
                dbContext.Set<T>().Update(entity);
                await dbContext.SaveChangesAsync();
                return entity;            
        }

        public async Task<bool> Delete(int id)
        {
            
                T entity = await dbContext.Set<T>().FirstOrDefaultAsync((e) => e.Id == id);
                dbContext.Set<T>().Remove(entity);
                await dbContext.SaveChangesAsync();
                return true;
            
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            
                IEnumerable<T> entities = await dbContext.Set<T>().ToListAsync();
                return entities;
           
        }

        public async Task<T> Get(int id)
        {
           
                T entity = await dbContext.Set<T>().FirstOrDefaultAsync((e) => e.Id == id);
                return entity;              
           
        }
    }
}
