﻿using Microsoft.EntityFrameworkCore;
using Net.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Net.Data
{
    public static class DBHelper
    {
        public static void Seed(NetDbContext netDbContext)
        {

            if (netDbContext.Regions.Any() ||
               netDbContext.Contacts.Any() ||
               netDbContext.Countries.Any() ||
               netDbContext.Markets.Any()) return;


            //Regions
            netDbContext.Add(
                new Region
                {
                    Code = "MU",
                    Id = 1,
                    Name = "Murcia"
                }
            );

            netDbContext.Add(
                new Region
                {
                    Code = "SF",
                    Id = 2,
                    Name = "Sheffield"
                }
              );

            //Contacts
              netDbContext.Add(
             new Contact
             {
                 Name = "David Martin",
                 Code = "DM",
                 Id = 1                 
             });

           netDbContext.Add(
            new Contact
             {
                 Name = "Alberto López",
                 Code = "AL",
                 Id = 2
             } );

           netDbContext.Add(
           new Contact
            {
                Name = "David Fernández",
                Code = "DF",
                Id = 3
            });

          netDbContext.Add(
          new Contact
            {
                Name = "Alberto Fernández",
                Code = "AF",
                Id = 4
            });

            //Countries
            netDbContext.Add(
        new Country
          {
              BackupContactId = 2,
              ContactId =1,
              Code ="ES",
              Name = "España",
              Id =1,
              RegionId = 1
          });

         netDbContext.Add(
       new Country
          {
              BackupContactId = 4,
              ContactId = 3,
              Code = "UK",
              Name = "United Kingdom",
              Id = 2,
              RegionId = 2
          });

          netDbContext.Add(
         new Market
           {
             Code = "español",
            CountryId = 1,
            Id = 1
           });

         netDbContext.Add(
           new Market
              {
                  Code = "ingles",
                  CountryId = 1,
                  Id = 2
              });


            netDbContext.Add(
       new Market
       {
           Code = "ruso",
           CountryId = 2,
           Id = 3
       });

            netDbContext.SaveChanges();
        }
    }
}
